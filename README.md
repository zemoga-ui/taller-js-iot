TALLER "JavaScript y el internet de las cosas"
----------------------------------------------
Elaborado con <3 por Zemoga.

El objetivo del taller es generar en los estudiantes curiosidad sobre las herramientas y las posibilidades que permite el desarrollo web, más específicamente, los lenguajes de programación relevantes para el mismo (HTML, CSS y JavaScript).

Se conoce como Internet de las Cosas (IoT, por sus siglas en inglés) a todas aquellas tecnologías que permiten la interacción entre internet y elementos disponibles en el plano físico, ya sea mecánicos (como puertas, ventanas, cerraduras, etc), eléctricos (luces, licuadoras, estufas, etc) e incluso electrónicos (televisores, radios, consolas de videojuegos, etc). Dispositivos tales como **Arduino, Raspberry Pi, Intel Edison** y otros, permiten que este acercamiento sea accesible para cualquier persona, haciendo que las posibilidades para conectar ambos mundos sean tan diversas como las ideas que en todas partes del mundo ya se están desarrollando.

Es nuestra responsabilidad como creadores de contenidos digitales acercar a nuestros usuarios de una manera amigable, fluida y confiable a las ideas que desarrollamos. Así pues, las experiencias físicas que se conectan con lo virtual deben estar encaminadas a lograr un uso adecuado de nuestras tecnologías, de la misma manera en que generemos satisfacción y gusto por ellas. 

Mediante este taller, los estudiantes participan de una experiencia interactiva, en donde exploraremos, a través de un simple circuito elaborado con un **Arduino**, de qué manera la experiencia de usuario juega un papel fundamental en la conexión con estos dos mundos, como también veremos algunas de las muchas posibilidades que las tecnologías web nos abren para que seamos partícipes en la creación de contenidos digitales relevantes y significativos para nuestro entorno.