/**
 * Z-House
 * Developed with <3 by Zemoga
 *
 * This is a client code to control Z-House
 * @author Andrés Zorro <andres.zorro@zemoga.com>
 */
(function(){
'use strict';

window.zHouse = typeof window.zHouse === 'function' ?
  window.zHouse : function(host) {
  var HOST = host || 'http://localhost:8000/',
      ROOMS = {
        livingRoom: ['light'],
        kitchen: ['light', 'fan'],
        bedroom: ['light', 'temperature'],
        bathroom: ['colorLight'],
        hall: ['door', 'alarm']
      },
      ITEMS = {
        light: ['on', 'off'],
        fan: ['speed'],
        colorLight: ['on', 'off', 'color'],
        alarm: ['on', 'off'],
        door: ['open', 'close'],
        temperature: ['read']
      },
      API = Object.create(null);

  /**
   * Handles GET requests
   * @param  {string}  room  Room name
   * @param  {string}  item  Item to act on
   * @return {Promise}       Async fetch promise
   */
  function getApi(room, item) {
    var req = {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }

    return fetch(HOST + room + '/' + item)
      .then(function(res) {
        return res.json();
      });
  }

  /**
   * Handles POST requests
   * @param  {string}  room    Room name
   * @param  {string}  item    Item to act on
   * @param  {string}  action  Action to be performed
   * @param  {*}       [param] Optional parameter to be taken (needed in some cases)
   * @return {Promise}         Async fetch promise
   */
  function postApi(room, item, action, param) {
    var req = {
      method: 'post',
      body: {},
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }, p;

    p = ['on', 'off', 'open', 'close'].indexOf(action) > -1 && 'state';
    item = item === 'colorLight' ? 'light' : item;

    if (action === 'color' && param) {
      if (typeof param === 'string') {
        p = 'hex';
      } else if (typeof param === 'object') {
        p = 'color';
      }
    }

    if (action === 'speed' && param) {
      p = 'speed';
    }

    if (!p) {
      throw new Error('Check again. You\'re missing something');
    }
    
    req.body[p] = param || action;
    req.body = JSON.stringify(req.body);

    return fetch(HOST + room + '/' + item, req)
      .then(function(res) {
        return res.json();
      });
  }

  Object.keys(ROOMS).forEach(function (room) {
    var opts = Object.create(null);

    ROOMS[room].forEach(function(item) {
      var actions = Object.create(null);

      ITEMS[item].forEach(function (action) {
        if (action === 'read') {
          actions.read = function() {
            return getApi(room, item);
          }
        } else {
          actions.status = function() {
            return getApi(room, item);
          }
          actions[action] = function(param) {
            return postApi(room, item, action, param);
          };
        }
      });

      opts[item] = actions;
    });

    API[room] = opts;
  });

  return API;
}

}());