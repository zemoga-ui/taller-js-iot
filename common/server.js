'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var debug = require('debug')('taller-iot:api');

var app = express();

// Constants
var PORT = 8000;

// Express middleware
app.use(bodyParser.json());
app.use(require('cors')());

app.get('/:houseRoom/:element', function (req, res) {
  var endpoint = req.params.houseRoom + '/' + req.params.element;
  debug('GET:', endpoint);
  try {
    require('api/' + endpoint).get.call(board, req, res);
    return;
  } catch(err) {
    debug('API Error', err);
    res.status(404).send({
      error: 'API error',
      trace: JSON.stringify(err)
    });
  }
});

app.post('/:houseRoom/:element', function (req, res) {
  var endpoint = req.params.houseRoom + '/' + req.params.element;
  debug('POST:', endpoint, req.body);
  try {
    require('api/' + endpoint).post.call(board, req, res);
    return;
  } catch(err) {
    debug(err);
    res.status(404).send({
      error: 'API error',
      trace: JSON.stringify(err)
    });
  }
});

app.all('/*', function(req, res) {
  res.send('Welcome to Z-House API!');
});

// Start server
app.listen(PORT, function() {
  console.log('Server started on port: %d', PORT);
});