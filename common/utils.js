'use strict';

var Q = require('q');

var POSSIBLE_STATES = {
  on: ['on', true, 'prendido', 'true', 'open'],
  off: ['off', false, 'apagado', 'false', 'close']
};

function _componentToHex(c) {
  var hex = c.toString(16);
  return hex.length === 1 ? '0' + hex : hex;
}

function rgbToHex(r, g, b) {
  return '#' + _componentToHex(r) + _componentToHex(g) + _componentToHex(b);
}

function hexToRgb(hex) {
  hex = hex.replace('#', '');

  if (hex.length !== 6) {
    return false;
  }

  return {
    red: parseInt(hex.substr(0, 2), 16),
    green: parseInt(hex.substr(2, 2), 16),
    blue: parseInt(hex.substr(4, 2), 16)
  };
}

function isValidValue(value) {
  var accepted = [
    'string',
    'boolean',
    'number',
    'object'
  ];
  if (accepted.indexOf(typeof value) > -1) {
    if (value !== null) {
      return true;
    }
  }

  return false;
}

function analogTo256(val) {
  return Math.floor(val * 255 / 1023);
}

function pinStatus(pin) {
  var q = Q.defer();

  // Get Light status and return JSON
  pin.query(function(pinState) {
    return q.resolve(pinState);
  });

  return q.promise;
}

function noop() {
  
}

function getStateName(body) {
  var state = body && 'state' in body;

  if (!state) {
    return false;
  }

  Object.keys(POSSIBLE_STATES).forEach(function(key) {
    if (POSSIBLE_STATES[key].indexOf(body.state) > -1) {
      state = key;
    }
  });

  return state;
}

module.exports = {
  analogTo256: analogTo256,
  pinStatus: pinStatus,
  rgbToHex: rgbToHex,
  hexToRgb: hexToRgb,
  isValidValue: isValidValue,
  getStateName: getStateName,
  noop: noop
};
