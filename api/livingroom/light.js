'use strict';

var NAME = 'Living Room';
var LIGHT_PIN = 11;
var light = require('elements/light')(LIGHT_PIN, NAME);

module.exports = light;
