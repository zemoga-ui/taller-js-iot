'use strict';

var NAME = 'Kitchen';
var LIGHT_PIN = 2;
var light = require('elements/light')(LIGHT_PIN, NAME);

module.exports = light;
