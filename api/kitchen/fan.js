'use strict';

var NAME = 'Kitchen';
var FAN_PIN = 9;
var fan = require('elements/fan')(FAN_PIN, NAME);

module.exports = fan;