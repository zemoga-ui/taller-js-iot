'use strict';

var NAME = 'Bathroom';
var LIGHT_PIN = {
  red: 3,
  green: 5,
  blue: 6
};
var light = require('elements/light')(LIGHT_PIN, NAME);

module.exports = light;
