'use strict';

var NAME = 'Common';
var PIEZO_PIN = 8;
var PSENSOR_PIN = 'A0';

var alarm = require('elements/alarm')(PIEZO_PIN,PSENSOR_PIN, NAME);

module.exports = alarm;
