'use strict';

var NAME = 'Common';
var DOOR_PIN = 10;
var door = require('elements/door')(DOOR_PIN, NAME);

module.exports = door;
