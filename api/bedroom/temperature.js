'use strict';

var NAME = 'Bedroom';
var TSENSOR_PIN = 'A1';
var temperature = require('elements/temperature')(TSENSOR_PIN, NAME);

module.exports = temperature;