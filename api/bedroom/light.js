'use strict';

var NAME = 'Bedroom';
var LIGHT_PIN = 12;
var light = require('elements/light')(LIGHT_PIN, NAME);

module.exports = light;
