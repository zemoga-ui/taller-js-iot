'use strict';

// App Module Path
require('app-module-path').addPath(__dirname);

var five = require('johnny-five');
var board = new five.Board();

board.on('ready', function() {
  console.log('Board Ready. Starting Server...');
  require('common/server');
});
