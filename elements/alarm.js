'use strict';

var five = require('johnny-five');
var Q = require('q');
var logger = require('debug');
var utils = require('common/utils');

module.exports = function(piezoPinNo,sensorPinNo, name) {
  var alarm;
  var photoresistor;
  var isActive = false;
  var debug = logger('taller-iot:' + name.toLowerCase().replace(/\s+/, '') + ':alarm');

  if (!utils.isValidValue(piezoPinNo) || !utils.isValidValue(sensorPinNo)) {
    throw 'No PIN ID assigned';
  }

  name = name || 'unnamed';
    
  alarm = new five.Piezo(piezoPinNo);
    
  photoresistor = new five.Sensor({
    pin: sensorPinNo,
    freq: 1000
  });

  return {
    get: function(req, res) {
      debug('GET');
      Q.when({
        pin: sensorPinNo,
        name: name,
        state: isActive ? 'active' : 'inactive'
      })
        .then(function(response) {
          debug('response', response);
          res.status(200).send(response);
        });
    },
    post: function(req, res) {
      debug('POST');
      isActive = utils.getStateName(req.body) === 'on';
      photoresistor.on('data', function() {
        var val = this.value;
        // console.log(val);
        if(isActive && val <= 600) {
          alarm.frequency(440, 1000);
        }
      });

      Q.when({
        pin: sensorPinNo,
        name: name,
        state: isActive ? 'active' : 'inactive'
      })
        .then(function(response) {
          debug('response', response);
          res.status(200).send(response);
        });
    }
  };

};
