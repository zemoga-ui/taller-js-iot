'use strict';

var five = require('johnny-five');
var Q = require('q');
var logger = require('debug');
var utils = require('common/utils');

var VALUES = {
    'high': 254,
    'medium': 128,
    'low':64,
    'off': 0 
};

function getSpeedName(body) {
  var speed = body && body.speed;

  if (!speed) {
    return false;
  }

  return VALUES[speed] || VALUES.off;
}

function setFanSpeed(board, fan, name, speed) {
  var q = Q.defer(),
      speedVal = getSpeedName(speed);

  if (typeof speedVal === 'number') {
    // Set motor status
    //board.pinMode(fan.pin, five.Pin.PWM);
    //board.analogWrite(fan.pin, speedVal);
    fan.start(speedVal);

    return Q.when({
      pin: fan.pin,
      name: name,
      speed: speed
    });
  }

  return Q.reject({
    pin: fan.pin,
    name: name,
    err: 'Speed value is invalid'
  });
}

module.exports = function(pinNo, name) {
  var fan;
  var debug = logger('taller-iot:' + name.toLowerCase().replace(/\s+/, '') + ':fan');

  if (!utils.isValidValue(pinNo)) {
    throw 'No PIN ID assigned';
  }

  name = name || 'unnamed';
  fan = new five.Motor({
    pin: pinNo
  });

  return {
    get: function(req, res) {
      debug('GET');
    },
    post: function(req, res) {
      var board = this;

      debug('POST', req.body.speed);
      setFanSpeed(board, fan, name, req.body)
        .then(function(response) {
          debug('response', response);
          res.status(200).send(response);
        });
    }
  }

};
