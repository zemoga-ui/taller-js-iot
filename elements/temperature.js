'use strict';

var five = require('johnny-five');
var Q = require('q');
var logger = require('debug');
var utils = require('common/utils');

function getTemperature(termo, name) {
  return utils.pinStatus(termo)
    .then(function(pin) {
      var temp = (((pin.value * 5) / 1024 - 0.5) * 100).toFixed(2);
      return {
        pin: termo.pin,
        name: name,
        state: temp + ' ºC'
      };
    })
    .fail(function(err){
      return {
        pin: termo.pin,
        name: name,
        error: err
      };
    });
}

module.exports = function(pinNo, name) {
    var termo;
    var debug = logger('taller-iot:' + name.toLowerCase().replace(/\s+/, '') + ':temperature');

    if (!utils.isValidValue(pinNo)) {
      throw 'No PIN ID assigned';
    }
    // Termo sensor
    termo = new five.Pin(pinNo);

    name = name || 'unnamed';

  return {
    get: function(req, res) {
      debug('GET');
      getTemperature(termo, name)
        .then(function(response) {
          debug('response:', response);
          return res.status(200).send(response);
        });
    }
  };
};
