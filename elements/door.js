'use strict';

var five = require('johnny-five');
var Q = require('q');
var logger = require('debug');
var utils = require('common/utils');

module.exports = function(pinNo, name) {
  var door;
  var isOpen = false;
  var debug = logger('taller-iot:' + name.toLowerCase().replace(/\s+/, '') + ':door');

  if (!utils.isValidValue(pinNo)) {
    throw 'No PIN ID assigned';
  }

  name = name || 'unnamed';
  door = new five.Servo({
    pin: pinNo,
    range: [5, 90],
    step: 5
  });

  door.min();

  return {
    get: function(req, res) {
      debug('GET');
      Q.when({
        pin: pinNo,
        name: name,
        state: isOpen ? 'open' : 'closed'
      })
        .then(function(response) {
          debug('response', response);
          res.status(200).send(response);
        });
    },
    post: function(req, res) {
      debug('POST');
      isOpen = utils.getStateName(req.body) === 'on';

      if (isOpen) {
        door.max();
      } else {
        door.min();
      }

      Q.when({
        pin: pinNo,
        name: name,
        state: isOpen ? 'open' : 'closed'
      })
        .then(function(response) {
          debug('response', response);
          res.status(200).send(response);
        });
    }
  };

};
