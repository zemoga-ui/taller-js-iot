'use strict';

var five = require('johnny-five');
var Q = require('q');
var logger = require('debug');
var utils = require('common/utils');

function getColor(body) {
  var color = body.color ? body.color : (body.hex && utils.hexToRgb(body.hex));

  if (!color) {
    return false;
  }

  return color;
}

function getSingleLightStatus(light, name) {
  return utils.pinStatus(light)
    .then(function(pin) {
      return {
        pin: light.pin,
        name: name,
        state: pin.state ? 'on' : 'off'
      };
    })
    .fail(function(err){
      return {
        pin: light.pin,
        name: name,
        error: err
      };
    });
}

function setSingleLightStatus(light, name, state) {
  var set = state ? 'high' : 'low';

  // Set light status
  light[set]();

  return Q.when({
    pin: light.pin,
    name: name,
    state: state ? 'on' : 'off'
  });
}

function getColorLightStatus(light, name) {
  var promises = [
      utils.pinStatus(light.red),
      utils.pinStatus(light.green),
      utils.pinStatus(light.blue)
    ];

  return Q.all(promises)
    .then(function (responses) {
      var color = responses.map(function(c) {
        return c.value;
      });

      color = {
        red: color[0],
        green: color[1],
        blue: color[2]
      };

      return {
        pins: [light.red.pin, light.green.pin, light.blue.pin],
        name: name,
        state: !color.red && !color.green && !color.blue ? 'off' : 'on',
        color: {
          rgb: color,
          hex: utils.rgbToHex(color.red, color.green, color.blue)
        }
      };
    });
}

function setColorLightStatus(light, name, color) {
  var board = this;

  board.pinMode(light.red.pin, five.Pin.PWM);
  board.pinMode(light.green.pin, five.Pin.PWM);
  board.pinMode(light.blue.pin, five.Pin.PWM);

  board.analogWrite(light.red.pin, color.red);
  board.analogWrite(light.green.pin, color.green);
  board.analogWrite(light.blue.pin, color.blue);

  return Q.when({
    pins: [light.red.pin, light.green.pin, light.blue.pin],
    name: name,
    state: !color.red && !color.green && !color.blue ? 'off' : 'on',
    color: {
      rgb: color,
      hex: utils.rgbToHex(color.red, color.green, color.blue)
    }
  });
}

module.exports = function(pinNo, name) {
  var light;
  var isRGB = false;
  var debug = logger('taller-iot:' + name.toLowerCase().replace(/\s+/, '') + ':light');

  if (!utils.isValidValue(pinNo)) {
    throw 'No PIN ID assigned';
  }

  if (typeof pinNo === 'object') {
    if (!utils.isValidValue(pinNo.red) ||
        !utils.isValidValue(pinNo.green) ||
        !utils.isValidValue(pinNo.blue)) {
      throw 'RGB Pin cannot be set';
    }

    isRGB = true;

    // RGB Led
    light = {
      red: new five.Pin(pinNo.red),
      green: new five.Pin(pinNo.green),
      blue: new five.Pin(pinNo.blue)
    };
  } else {
    // Common Led
    light = new five.Pin(pinNo);
  }

  name = name || 'unnamed';

  if (isRGB) {
    return {
      get: function(req, res) {
        debug('GET');
        getColorLightStatus(light, name)
          .then(function(response) {
            debug('response:', response);
            return res.status(200).send(response);
          });
      },
      post: function(req, res) {
        var board = this;
        var state = utils.getStateName(req.body) === 'on' ? 255 : 0;
        var color = getColor(req.body) || {
          red: state,
          green: state,
          blue: state
        };

        if (utils.getStateName(req.body) === 'off') {
          color = {
            red: state,
            green: state,
            blue: state
          };
        }

        debug('POST', color);
        setColorLightStatus.call(board, light, name, color, state)
          .then(function(response) {
            debug('response:', response);
            return res.status(200).send(response);
          });
      }
    };
  }

  return {
    get: function(req, res) {
      debug('GET');
      getSingleLightStatus(light, name)
        .then(function(response) {
          debug('response:', response);
          return res.status(200).send(response);
        });
    },
    post: function(req, res) {
      var state = utils.getStateName(req.body);

      if (!state) {
        return res.status(400).send({
          error: 'Bad Request',
          description: 'Missing "state" param.'
        });
      }

      state = state === 'on' ? true : false;

      debug('POST', state);
      setSingleLightStatus(light, name, state)
        .then(function(response) {
          debug('response:', response);
          return res.status(200).send(response);
        });
    }
  };
};
